smooth_rod_d      = 10;
threaded_rod_d    = 10;
tie_wrap_w        = 5;
tie_wrap_t        = 2;
washer_d          = 21;
height_of_threaded = 26.5;
height_of_smooth = height_of_threaded + 11 + 9;
height_of_post = height_of_smooth+2;

belt_width = 6.5;
belt_tooth_distance = 2;
belt_tooth_ratio = 0.5;
belt_thickness = 0.8;

idler_bearing_inner_d = 4; // 624 bearing

bearing_diameter = 19;
bearing_cut_extra = 0.2; // padding so it's not too tight

zmotor_delta_x = 5;
zmotor_delta_y = -3;

// settings...
rod_distance = 50;
x_box_height = 5 + rod_distance + smooth_rod_d;
