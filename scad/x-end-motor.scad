// PRUSA iteration3
// X end motor
// GNU GPL v3
// Josef Průša <iam@josefprusa.cz> and contributors
// http://www.reprap.org/wiki/Prusa_Mendel
// http://prusamendel.org
// Alterations for reprap Wilson by Martin Rice <mrice411@gmail.com>

include <configuration.scad>
use <x-end.scad>

offs_adjuster_y = 5.5;
adj_block_x = 9;
adj_block_y = 10;
adj_block_z = 32;
motor_offs_z = 0;

motor_y_offset = 7;

module adjustomatic() {
translate ([0, 0, 6.5])
   difference() {     
       translate(v=[-(15+17/2+adj_block_x/2),offs_adjuster_y,58.5-adj_block_z/2])
          cube(size=[adj_block_x,adj_block_y,adj_block_z],center=true);
       
       translate(v=[-(15+17/2+adj_block_x/2)-5,offs_adjuster_y,58-adj_block_z/2-8]) 
         rotate([0,-30,0]) cube(size=[adj_block_x,adj_block_y+2,adj_block_z],center=true);

       translate(v=[-(15+17/2+adj_block_x/2-1),offs_adjuster_y,58-adj_block_z/2-3]) 
          cube(size=[adj_block_x,adj_block_y-2,adj_block_z-2],center=true);

       translate(v=[-(15+17/2+adj_block_x/2),offs_adjuster_y,58-adj_block_z/2+14]) 
           {
         rotate([0,0,30]) #cylinder(h = 4, r = 7.0/2 , $fn = 6);
         translate([0,0,-20]) #cylinder(h=30,r=1.6,$fn=16);
}

   }

}

// the endstop mount will be translated by this much (negative values
// mean it moves away from the motor and toward the X carriage, giving
// more room for leads and also extra space for the auto bedleveling
// servo housing which is on this side of the extruder):
endstop_sw_offs_adjust = -0;

module pocket_endstop() // endstop holder grafted onto the side toward the rods
{
  translate([-7,-45,0]) 
    union() {
      // angled wall that attaches to the endstop holder
      translate([-1,18,0]) rotate([0,0,-30]) cube(size=[8.5,2,22]);
      // little bit of extra support at the bottom corner 
      translate([-1.5,15.,2]) rotate([0,90,0]) cube(size=[2,3,10]);
      
      difference() {
       translate([-1.5,endstop_sw_offs_adjust,0]) cube(size=[9,20.5,22]);
           
       translate([-2,endstop_sw_offs_adjust,1]) cube(size=[7,21,22]);
       translate([0,15.5,8]) cube(size=[10,5,20]);

       // screw holes for endstop switch
       translate([-2,7+endstop_sw_offs_adjust,1.5+5.5]) rotate([0,90,0]) cylinder(r=1.5,h=15);
       translate([-2,7+endstop_sw_offs_adjust,1.5+5.5+9.5]) rotate([0,90,0]) cylinder(r=1.5,h=15);
      }
    }
}

module x_end_motor_sr() {
     // TODO: use x_box_height
    difference() { 
       cube(size=[8,14+motor_y_offset,12]);
       #translate([4,13.5+motor_y_offset,11]) rotate([0,90,0]) difference() {
	    cylinder(r=4.5,h=4,$fn=16,center=true);
	    translate([0,0,-1]) cylinder(r=2.5,h=7,$fn=16,center=true);
       }
       #translate([9,12.6+motor_y_offset,12]) rotate([0,45,90]) cube(size=[2,11,2]);
    }
}

module x_end_motor_base(){
 x_end_base();
 // motor arm
 translate(v=[-15,31,26.5+motor_offs_z]) cube(size = [17,44+motor_y_offset*2,53], center = true);
 // z stop adjuster
 adjustomatic();
 // x endstop holder
 pocket_endstop();
}

screw_head_r = 3.5;

module x_end_motor_holes(){
 x_end_holes();
 // Position to place
 translate(v=[-1,32,30.25+motor_offs_z]){
  // Belt hole
  translate(v=[-14,1,0]) cube(size = [10,46,22], center = true);
  translate ([0, motor_y_offset, 0]) {
  // Motor mounting holes
  translate(v=[20,-15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 70, r=1.8, $fn=30);
  translate(v=[1,-15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 12, r=screw_head_r, $fn=30);
 

  translate(v=[20,-15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 70, r=1.8, $fn=30);
  translate(v=[1,-15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 12, r=screw_head_r, $fn=30);


  translate(v=[20,15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 70, r=1.8, $fn=30);
  translate(v=[1,15.5,-15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 12, r=screw_head_r, $fn=30);


  translate(v=[20,15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 70, r=1.8, $fn=30);
  translate(v=[1,15.5,15.5]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 12, r=screw_head_r, $fn=30);

  // Material saving cutout 
  translate(v=[-10,12,10]) cube(size = [60,42,42], center = true);

  // Material saving cutout
  translate(v=[-10,40,-30]) rotate(a=[45,0,0])  cube(size = [60,42,42], center = true);
  // Motor shaft cutout
  translate(v=[0,0,0]) rotate(a=[0,-90,0]) rotate(a=[0,0,90]) cylinder(h = 70, r=17, $fn=6);
}
  // zip tie retainer for securing end stop wiring
  #translate([-5,-63,14]) difference() { cylinder(r=4.5,h=4,$fn=16);
                                          translate([0,0,-1]) cylinder(r=2.5,h=7,$fn=16);
                                        }
  #translate([-5,-48,x_box_height-30.5]) rotate([90,0,0]) difference() { cylinder(r=4.5,h=4,$fn=16,center=true);
       translate([0,0,-1]) cylinder(r=2.5,h=7,$fn=16,center=true);
  }
 }
}

// Final part
module x_end_motor(){
 difference(){
  x_end_motor_base();
  x_end_motor_holes();
 }
 // strain relief (zip tie point) below the motor
 translate([-23.5,9,53]) x_end_motor_sr();
}

rotate([0,0,180])
x_end_motor();
