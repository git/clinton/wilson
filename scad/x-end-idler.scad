// PRUSA iteration3
// X end idler
// GNU GPL v3
// Josef Průša <iam@josefprusa.cz> and contributors
// http://www.reprap.org/wiki/Prusa_Mendel
// http://prusamendel.org
// Alterations for reprap wilson by M. Rice <mrice411@gmail.com>

include <configuration.scad>
use <x-end.scad>

idler_offs_z = -1; // negative here means "up" when installed
idler_offs_y = 4;

center_z = 30.25 -1;
tensioner_size_z = 12;

module x_end_idler_base(){
 x_end_base();
}

module x_end_idler_holes(){
 x_end_holes();
 translate([0,idler_offs_y,idler_offs_z]) {
      #translate(v=[0,-22,30.25]) rotate(a=[0,-90,0]) cylinder(h = 80, r=idler_bearing_inner_d/2+.3, $fn=30);
      #translate(v=[2,-22,30.25]) rotate(a=[0,-90,0]) cylinder(h = 10, r=idler_bearing_inner_d/2 + 1, $fn=30);
      #translate(v=[-21.5,-22,30.25]) rotate(a=[0,-90,0]) rotate(a=[0,0,30]) cylinder(h = 80, r=idler_bearing_inner_d, $fn=6);

      // create a notch for the X tensioner, to improve the length of
      // travel available
      translate (v=[0,-22,30.25]) translate(v=[-10,-20,1]) #difference()  {
	   rotate(a=[45,0,0])  cube(size=[30,22,22],center=true); 
	   translate(v=[0,14,0]) cube(size=[31,4,8],center=true);
      }
 }
}
 
// Final part
module x_end_idler(){
 mirror([0,1,0]) difference(){
  x_end_idler_base();
  x_end_idler_holes();
 }

 // added ridges to keep the tensioner from pitching
 for (x = [-9.8, -20.2], z = [center_z-tensioner_size_z/2 - .5, center_z+tensioner_size_z/2 + .5]) {
      d = 2;
      translate(v=[x,1, z]) intersection () {
	   rotate ([45, 0, 90]) cube(size=[20,d, d],center=true);
	   translate ([(x < -15) ? d  : -d, 0, 0]) rotate ([0, 0, 90]) cube(size=[20,d*2, d*2],center=true);
      }
 }
}

x_end_idler();


