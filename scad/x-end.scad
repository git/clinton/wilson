// PRUSA iteration3
// X end prototype
// GNU GPL v3
// Josef Průša <iam@josefprusa.cz> and contributors
// http://www.reprap.org/wiki/Prusa_Mendel
// http://prusamendel.org

// todo: make x-end height global and make anything relying implicitly
// on the height of the x-end use that for positioning.

// bearing height, strain relief (&& maybe make dependent upon the bearing diameter)



// major issues with rev 3

// - motor side screw hole conflicts with switch / other rear mount is
//   inaccessible without removing the leadscrew from the nut

// - New idler anti-pitching ribs are too big (new idler I wasn't
//   using is larger so the existing ones worked fine for that one...)
//   NOTE: new idler is meant for integrated toothed idler...

use <bearing.scad>
include <configuration.scad>

pushfit_d = smooth_rod_d + 0.25;
hex_nut_r = 4.75;

module nut_trap_base () {
     cube(size=[16,21,8],center=true);
     translate ([2,0,0]) cylinder(h=8,r=12.5,$fn=50,center=true);
}

module nut_trap () {
     union() { 
               // center post of brass nut
                   cylinder(h=12,r=5.45,$fn=50,center=true);
                   // holes for m3 screws in brass nut
                   translate(v=[8,0,0]) cylinder(h=12,r=1.8,$fn=20,center=true);
		   #rotate([0,0,90]) translate(v=[8,0,0]) cylinder(h=12,r=1.8,$fn=20,center=true);
		   rotate([0,0,180]) translate(v=[8,0,0]) cylinder(h=12,r=1.8,$fn=20,center=true);
		   #rotate([0,0,270]) translate(v=[8,0,0]) cylinder(h=12,r=1.8,$fn=20,center=true);
     }
}

module x_end_base(){
// Main block
height = x_box_height;
// TODO: calculate correct translate(y) + cube(y) using size of bearing
translate(v=[-15,-10,height/2]) cube(size = [17,45,height], center = true);
// Bearing holder
 vertical_bearing_base();	
//Nut trap
 translate(v=[-4-zmotor_delta_y,-17-zmotor_delta_x,4]) nut_trap_base ();
 // Cube
// #translate(v=[-2-2,-17,4]) cube(size = [8,16,8], center = true);
 // Hexagon
// #translate(v=[0,-17,0]) rotate([0,0,30]) cylinder(h = 8, r=8, $fn = 6);
}

module x_end_holes(){
 vertical_bearing_holes();
 // Belt hole
 translate(v=[-1,0,0]){
 // Stress relief
 translate(v=[-5.5-10+1.5,-bearing_diameter/2-4,30]) cube(size = [20,1,28], center = true);
 // Cut out for belt (this is NOT centered because the motor isn't centered)
 difference(){
	translate(v=[-5.5-10+1.5,-10,30]) cube(size = [10,46,28], center = true);

	// Nice edges
	translate(v=[-5.5-10+1.5,-10,30+23]) rotate([0,45,0]) cube(size = [10,46,28], center = true);
	translate(v=[-5.5-10+1.5,-10,30+23]) rotate([0,-45,0]) cube(size = [10,46,28], center = true);
	translate(v=[-5.5-10+1.5,-10,30-23]) rotate([0,45,0]) cube(size = [10,46,28], center = true);
	translate(v=[-5.5-10+1.5,-10,30-23]) rotate([0,-45,0]) cube(size = [10,46,28], center = true);

}
}


 pushrod_extra_z = 2.5;
// Bottom pushfit rod
#translate(v=[-15,-41.5,smooth_rod_d/2+pushrod_extra_z]) rotate(a=[-90,0,0]) pushfit_rod(pushfit_d,50);
// Top pushfit rod
translate(v=[-15,-41.5,rod_distance+smooth_rod_d/2+pushrod_extra_z]) rotate(a=[-90,0,0]) pushfit_rod(pushfit_d,50);
// Nut trap
translate(v=[-zmotor_delta_y,-17-zmotor_delta_x,3]) rotate ([0, 0, 45]) nut_trap ();
// #translate(v=[0,-17,-0.5]) cylinder(h = 4, r1=3.4, r2=2.9, $fn=25);
// translate(v=[0,-17,3]) rotate([0,0,30]) cylinder(h = 10, r=hex_nut_r, $fn = 6);
}


// Final prototype
module x_end_plain(){
 difference(){
  x_end_base();
  x_end_holes();
 }
}

x_end_plain();


module pushfit_rod(diameter,length){
 translate([0,-0.3,0])  cylinder(h = length, r=diameter/2, $fn=30);
 translate([0,0.3,0])  cylinder(h = length, r=diameter/2, $fn=30);
// difference(){
// 	translate(v=[0,-diameter/2.85,length/2]) rotate([0,0,45]) cube(size = [diameter/2,diameter/2,length], center = true);
// 	translate(v=[0,-diameter/4-diameter/2-0.4,length/2]) rotate([0,0,0]) cube(size = [diameter,diameter/2,length], center = true);
// }
 
}

